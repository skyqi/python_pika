from flask import Flask, request, jsonify
import pika
import time
from datetime import datetime
import argparse
import ffmpeg

app = Flask(__name__)

RABBITMQ_HOST = '82.156.207.54'
rtmp_url = "rtmp://82.156.207.54/live/livestream"
WEB_ROOT = "/www/wwwroot/static2.ggiooo.com"

@app.route("/")
def hello():
    return "Hello, World!"



# def RabbitMQSender3():
#     cwd = request.args.get('cwd')
#     file = request.args.get('file')
#     filename = f"{cwd}/{file}"
#     return filename



@app.route('/on_rabbitmqsender', methods=['POST'])
def on_rabbitmqsender():
    # cwd = request.args.get('cwd')
    data = request.get_json()
    file = data['file']
    routing_key = data['stream_url']
    if file == None or routing_key == None:
        return {'code': "1", 'msg': '参数错误'}
    filename = f"{file}"

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBITMQ_HOST))
    channel = connection.channel()
    channel.queue_declare(queue='srspostimage', durable=True)  # 队列
    channel.basic_publish(exchange='',
                          routing_key=routing_key,  # 路由键
                          body=filename,
                          properties=pika.BasicProperties(
                              delivery_mode=2,  # make message persistent
                          ))

    # print(f" [x] Sent  {filename}")
    connection.close()
    # print("0")
    return {'code': "0", 'msg': '成功'}


if __name__ == '__main__':
    from werkzeug.serving import run_simple

    run_simple('0.0.0.0', 5000, app)
