# sender.py
import os
import re
import subprocess

import pika
import time
from datetime import datetime

import ffmpeg

#定义常量
RABBITMQ_HOST = '82.156.207.54'
current_time = time.strftime('%Y%m%d%H%M%S')
file_name_pattern = f'stream_piece_{current_time}_%d.mp4'
# 创建分段文件
segment_files = []
for i in range(1000):  # 假设你想要创建 100 个分段
    segment_file = f'{current_time}_{i:03d}.mp4'
    segment_files.append(segment_file)

# FFmpeg命令行参数
ffmpeg_cmd = [
    # 指定FFmpeg的可执行文件路径（如果不在系统路径中）
    'ffmpeg',
    # 指定输入文件，这里使用RTMP流
    '-i', 'rtmp://82.156.207.54/live/livestream',
    # 指定输出文件格式为MP4，并使用复制（copy）编码方式
    '-c', 'copy',
    # 指定输出格式为分段（segment）
    '-f', 'segment',
    # 指定每个分段的时长为60秒
    '-segment_time', '5',
    '-s', '320x240',  # 设置视频尺寸为320x240
    # 指定分段格式为MP4
    '-segment_format', 'mp4',
    # 指定分段文件名格式为时间戳
     '-strftime', f'{current_time}_%d.mp4'
    # 输出分段文件列表
    '-write_playlist', '1',

]

rtmp_url = "rtmp://82.156.207.54/live/livestream"
# ffmpeg_cmd2 = f'ffmpeg -i {rtmp_url} -c copy -f segment -segment_time 60 stream_piece_%%s.mp4'



# 分阶压片的函数
# https://kkroening.github.io/ffmpeg-python/
# 开始拉流并分割文件
def start_streaming():

    current_time = time.strftime('%Y%m%d%H%M%S')
    file_name = f'stream_piece_{current_time}_%d.mp4'
    ffmpeg_cmd2 = f'ffmpeg -i {rtmp_url} -c copy -f segment -segment_list segment_list.txt -segment_time 5  {file_name}'

    process = (
        ffmpeg
            .input(rtmp_url)
            .output(file_name, **{'format': 'segment', 'segment_list': 'segment_list.txt', 'segment_time': 5})
            .run_async()
    )

    out, err = process.communicate()


def RabbitMQSender():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBITMQ_HOST))
    channel = connection.channel()

    channel.queue_declare(queue='hello',durable=True)

    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    # print(current_time)
    channel.basic_publish(exchange='',
                          routing_key='hello',
                          body=current_time,
                          properties=pika.BasicProperties(
                             delivery_mode = 2,  # make message persistent
                          ))

    print(f" [x] Sent  {current_time}")
    connection.close()



if __name__ == '__main__':
    RabbitMQSender()

