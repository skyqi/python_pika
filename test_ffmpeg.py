import datetime
import os
import subprocess
from scenedetect import VideoManager, SceneManager, ContentDetector
import common

in_filename = '/www/wwwroot/static2.ggiooo.com/video/live/livestream/20240425/1714053308544.mp4'
config = common.readConfig()
out_directory = config.get('scene_out_directory', 'root_directory')
out_directory = out_directory + config.get('scene_out_directory', 'out_img_directory')
out_directory = out_directory + datetime.datetime.now().strftime("%Y%m%d")
os.makedirs(out_directory, exist_ok=True)
_now = datetime.datetime.now().strftime("%H%M")
out_filename = f'{out_directory}/scene_{_now}.jpg'
ffmpeg_cmd = [
                'ffmpeg',
                '-i', in_filename,
                '-frames:v', '1',
                '-filter:v', 'scale=-1:360',
                '-q:v', '2',
                '-y',
                out_filename
            ]
result = subprocess.run(ffmpeg_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
print(result.stdout)
print(out_filename)

