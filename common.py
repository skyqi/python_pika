import base64
import configparser
import os

#当前绝对路径的函数
def get_current_absolute_path():
    return os.path.abspath(os.path.dirname(__file__))

def base64_image(imagepathfile):
    with open(imagepathfile, 'rb') as f:
        image_data = f.read()
        base64_data = base64.b64encode(image_data).decode('utf-8')
        return base64_data

def create_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def readConfig():
    Config = configparser.ConfigParser()
    # 读取 INI 文件
    Config.read('config.ini', 'utf8')
    return Config
