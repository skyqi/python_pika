# receiver.py
import pika
import time,os
import ffmpeg

#定义常量
RABBITMQ_HOST = '82.156.207.54'
# rtmp_url = "rtmp://82.156.207.54/live/livestream"

def pikaReceiver():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBITMQ_HOST)
    )
    # 声明队列 'user_01'（如果队列不存在，则创建它）
    channel = connection.channel()
    queue = 'on_dvr_scene_queue'
    channel.queue_declare(queue=queue, durable=True)

    def callback(ch, method, properties, body):
        print(f" [x] Received {body.decode()}")
        time.sleep(body.count(b'.'))
        print(" [x] Done")

        # out_filename = start_streaming(body.decode('utf-8'))
        # print(f" [x] Received {body}")
        # print(f" [x] Received {out_filename}")

    # 告诉RabbitMQ，使用上面定义的回调函数来接收 'user_01' 队列的消息
    # 这不是'srspostimage'
    channel.basic_consume(queue=queue,
                          on_message_callback=callback,
                          auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


# 分阶压片的函数
# https://kkroening.github.io/ffmpeg-python/
# 开始拉流并分割文件
def start_streaming(in_filename):

    # in_filename ='stream_piece_20240417123200_98.mp4';
    #要求判断文件是否存在的代码
    # 检查文件是否存在
    if os.path.exists(in_filename) is None:
        print(f"{in_filename}文件不存在")
        return None

    out_filename = in_filename.replace('.mp4', '.jpg')
    time = '00:00:03.0'  # 生成缩略图的时间点，格式为HH:MM:SS.ms
    width = 320  # 缩略图的宽度，高度会根据原始视频的比例自动计算

    # 创建FFmpeg命令
    ffmpeg_cmd = (
        ffmpeg
            .input(in_filename, ss=time)  # 指定输入视频文件和开始时间点
            .filter('scale', width, -1)  # 使用scale过滤器缩放视频尺寸
            .output(out_filename, vframes=1)  # 指定输出文件名和只保留第一帧
            .run()  # 执行FFmpeg命令
    )

    return out_filename



if __name__ == '__main__':
    pikaReceiver()

