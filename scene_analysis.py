# ***********
# 场景分析
# ***********
import json
import os

import common
import requests

from server.RabbitMqConsumer import RabbitMqConsumer
from server.ZhiPuAi import ZhiPuAi
from server import SceneDetectx
from datetime import datetime

config = common.readConfig()


def get_scene_txt(imagepathfile):
    print('请求AI.....')
    zhiPuAi = ZhiPuAi(imagepathfile)
    message = zhiPuAi.get_scene_txt()
    print('AI回复: ')
    print(message)
    return message.content


def store_scene(scene_msg, scene_img, scene_video, create_at):
    now = datetime.now()
    formatted_time = now.strftime("%Y-%m-%d %H:%M:%S")
    url = 'https://dou.ggiooo.com/api/v2/scene/store'
    # 要发送的数据
    data = {
        'scene_msg': scene_msg,
        'scene_img': scene_img,
        'scene_video': scene_video,
        'created_at': create_at,
        'last_at': formatted_time
    }
    Rabbit_publish(data)
    # 发起POST请求
    response = requests.post(url, data=data)
    # 打印响应内容
    print(response.text)


def Rabbit_publish(data):
    # 处理(新增)rtc_publisher_queue通道
    print('Rabbit_publish 事件 >>>')
    print(data)
    queue_name = 'rtc_publisher_queue'
    consumer = RabbitMqConsumer(queue_name)

    consumer.publish(json.dumps(data))


def on_drv_scene_callback(body):
    # jsonVal = json.loads(json.loads(body))
    jsonVal = json.loads(body)
    print("53 >>>>")
    print(jsonVal)
    directory = config.get('scene_out_directory', 'mp4_root_directory')
    file = directory + jsonVal['file']
    scene_analysis(file, jsonVal['create_at'])
    return False


def scene_analysis_callback(pathfileimage, scene_video, create_at):
    if pathfileimage is None:
        return
    # 判断文件是否存在
    if os.path.exists(pathfileimage):
        scene_msg = get_scene_txt(pathfileimage)
        if scene_msg:
            store_scene(scene_msg, pathfileimage, scene_video, create_at)
    else:
        print('出错！未找到生成的图片: ' + pathfileimage)
        return


def scene_analysis(in_filename, create_at):
    if os.name == 'nt':
        in_filename = common.get_current_absolute_path() + '/video/1714097987603.mp4'
    print("debuger 48>>>>>")
    print(in_filename)
    if os.path.exists(in_filename):
        scene_detector = SceneDetectx.SceneDetector(in_filename, create_at, scene_analysis_callback)
        scene_detector.process_video()
    else:
        print(f"line:86, 文件 '{in_filename}' 不存在。")
        return False


# def main3():
#     pass
# in_filename = common.get_current_absolute_path() + '/video/1714097987603.mp4'
# # in_filename = common.get_current_absolute_path() + '/video/video.mp4'
# def mycall(pathfileimage,scene_video):
#     print(pathfileimage)

# scene_detector = SceneDetectx.SceneDetector(in_filename, mycall,)
# scene_detector.process_video()

def main():
    queue_name = 'on_dvr_scene_queue'
    consumer = RabbitMqConsumer(queue_name)
    consumer.consume(on_drv_scene_callback)


if __name__ == '__main__':
    main()

