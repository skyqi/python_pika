import base64
import configparser
from zhipuai import ZhipuAI

import common


class ZhiPuAi:

    def __init__(self, imagepathfile):
        self.config = common.readConfig()
        self.api_key = self.config.get('chatgpt', 'zhipu_apikey')
        self.glm4v_url = 'https://open.bigmodel.cn/api/paas/v4/chat/completions'
        self.imagepathfile = imagepathfile

    def img2base64(self):
        with open(self.imagepathfile, 'rb') as f:
            image_data = f.read()
            base64_data = base64.b64encode(image_data).decode('utf-8')
            return base64_data

    def get_scene_txt(self):
        # 获取用户名列表
        client = ZhipuAI(api_key=self.api_key)  # 填写您自己的APIKey
        response = client.chat.completions.create(
            model="glm-4v",
            messages=[
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "text",
                            "text":  self.config.get('chat_messages', 'role_user')
                        },
                        {
                            "type": "image_url",
                            "image_url": {
                                "url": self.img2base64()
                            }
                        }
                    ]
                }
            ]
        )
        #判断下response是否有值
        if response:
            return response.choices[0].message
        else:
            return None
