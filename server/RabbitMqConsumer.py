import datetime
import time
import common
import requests
import pika

# from server.SceneDetectx import SceneDetector
config = common.readConfig()

RABBITMQ_HOST = config.get('scene_out_directory', 'RABBITMQ_HOST')


class RabbitMqConsumer:
    def __init__(self, queue):

        self.queue = queue
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=RABBITMQ_HOST
                , virtual_host='dou'
                , heartbeat=0  # 设置心跳间隔为0
            ))
        self.channel = self.connection.channel()
        # self.channel.queue_declare(self.queue,  False, False, False, False);
        _durable = config.get('scene_out_directory', 'durable')
        try:
            # passive passive设置为True时，队列声明不会创建队列，而是仅仅检查队列是否存在
            # passive参数为False，这意味着如果队列不存在，它将被创建。如果队列存在，则声明成功
            # auto_delete设置为True，队列将在最后一个消费者断开连接后自动删除。
            # auto_delete设置为False（默认值），队列是持久的，即使没有消费者，它也不会被删除。
            self.channel.queue_declare(queue=self.queue, durable=True, passive=False, auto_delete=True)

            print("__init__ RabbitMqConsumer >>> ")
        except Exception as e:
            print(f'line 31，Queue {self.queue} Error {e} !!!')
        else:
            pass

    # 生产者
    def publish(self, message):
        self.channel.basic_publish(exchange='', routing_key=self.queue, body=message)
        print(" [x] Sent %r" % message)

    def consume(self, callback):
        self.relcallback = callback
        # no_ack设置为False,可以选择不确认消息，这样消息就会留在队列中
        # no_ack设置为True（默认值），消费者会自动确认接收到的每个消息。这意味着一旦消费者成功处理了消息，消息就会被从队列中移除
        self.channel.basic_consume(
            queue=self.queue,
            on_message_callback=self.rabbitCallback,
            # auto_ack=config.get('scene_out_directory', 'auto_ack')
            auto_ack=True
        )
        print(' [*] Waiting for messages. To exit press CTRL+C')
        try:
            self.channel.start_consuming()

        except KeyboardInterrupt:
            print(' [*] Interrupted')
        except Exception as e:
            print(f'line:52,An error occurred: {e}')

    # 消费者
    def rabbitCallback(self, ch, method, properties, body):
        current_time = datetime.datetime.now().strftime("%H:%M:%S")
        print(f" [x] {current_time} Received '{body.decode()}'")
        # //回调先处理场景
        ret = self.relcallback(body.decode())
        if ret:
            # "Message acknowledged and removed from queue")
            ch.basic_ack(delivery_tag=method.delivery_tag)
            print("Message acknowledged and removed from queue!")

    def close(self):
        self.channel.stop_consuming()
        self.channel.close()
        self.connection.close()

# queue_name = 'on_dvr_scene_queue'
# consumer = RabbitMqConsumer(queue_name)
# consumer.consume()
# consumer.close()
