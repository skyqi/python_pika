# sender.py

import pika
import time
from datetime import datetime
import argparse
import ffmpeg

#定义常量
RABBITMQ_HOST = '82.156.207.54'

rtmp_url = "rtmp://82.156.207.54/live/livestream"


def geton_hlsApi():
    pass

def getArgument():
    # 创建 ArgumentParser 对象
    parser = argparse.ArgumentParser(description='Example with argparse')

    # 添加字符串类型的参数
    parser.add_argument('filename', type=str, help='A name to greet')

    # 解析参数
    args = parser.parse_args()

    # 使用参数
    print(f"Hello, {args.filename}!")
    return args.filename


def RabbitMQSender():

    filename = getArgument()

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBITMQ_HOST))
    channel = connection.channel()

    channel.queue_declare(queue='srspostimage',durable=True)   #队列

    filename = 'stream_piece_20240417123200_98.mp4'

    channel.basic_publish(exchange='',
                          routing_key='user_02', #路由键
                          body=filename,
                          properties=pika.BasicProperties(
                             delivery_mode=2,  # make message persistent
                          ))

    print(f" [x] Sent  {filename}")
    connection.close()



if __name__ == '__main__':
    RabbitMQSender()

