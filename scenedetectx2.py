from scenedetect import SceneManager, VideoManager
from scenedetect.detectors import ContentDetector
from scenedetect.video_splitter import split_video_ffmpeg

def detect_scenes_with_time_interval(video_manager, interval_seconds):
    # 创建一个空列表来存储场景时间点
    scene_times = []

    # 遍历视频帧
    for frame in video_manager.frame_gen():
        # 获取当前帧的时间戳
        current_time = frame.timestamp

        # 检查是否是时间间距的开始或结束
        if scene_times and (current_time - scene_times[-1]) > interval_seconds:
            # 如果时间间距足够大，添加新的场景时间点
            scene_times.append(current_time)

    # 返回场景时间点列表
    return scene_times

# 创建视频管理器和场景管理器
video_manager = VideoManager([video_path])
scene_manager = SceneManager()
scene_manager.add_detector(ContentDetector())

# 初始化视频管理器
video_manager.set_downscale_factor()
video_manager.start()

# 设置时间间距
interval_seconds = 10  # 例如，每10秒检测一次场景

# 检测场景变化
scene_times = detect_scenes_with_time_interval(video_manager, interval_seconds)

# 获取检测到的场景列表
scene_list = scene_manager.get_scene_list()

# 输出场景时间点
for scene_time in scene_times:
    print(f"Detected scene at time: {scene_time}")

# 清理资源
video_manager.release()
