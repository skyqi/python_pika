
import common
import requests
from server import zhipuaiclass


def get_scene_txt():
    imagepathfile = common.get_current_absolute_path() + '\\output\\scene_000.jpg'

    zhiPuAi = zhipuaiclass.ZhiPuAiClass(imagepathfile)
    message = zhiPuAi.get_scene_txt()
    print(message)


def store_scene(scene_msg,scene_img,scene_video):
    url = 'https://dou.ggiooo.com/api/v2/scene/store'
    # 要发送的数据
    data = {
        'scene_msg': scene_msg,
        'scene_img': scene_img,
        'scene_video': scene_video
    }

    # 发起POST请求
    response = requests.post(url, data=data)

    # 打印响应内容
    print(response.text)


