import datetime

from scenedetect import VideoManager, SceneManager, ContentDetector
from scenedetect.detectors import ContentDetector
import subprocess

# 创建视频管理器和场景管理器
in_filename = '../video/video.mp4'
out_directory = './output'

def get_scene():
    video_manager = VideoManager(in_filename)
    scene_manager = SceneManager()

    scene_manager.add_detector(ContentDetector())
    # 设置 frame-skip 参数


    video_manager.set_downscale_factor()
    video_manager.start()
    # 设置时间间距
    interval_seconds = 60  #间隔多长时间取场景
    # 设置内容检测的阈值

    scene_manager.detect_scenes(
        frame_source=video_manager
    )
    scene_list = scene_manager.get_scene_list()
    time_format = "%H:%M:%S"
    # 初始化转换后的秒数列表
    filtered_time_series = []
    previous_time = None
    for i, scene in enumerate(scene_list):
        end_frame = str(scene[1])
        # 去除小数点及其后面的部分
        end_frame_str = end_frame.split('.')[0]
        if i==0:
            current_time = datetime.datetime.strptime(end_frame_str, time_format)
        else:
            previous_time = datetime.datetime.strptime(end_frame_str, time_format)

        if previous_time is not None:
            time_delta = current_time - previous_time
            if abs(time_delta.total_seconds()) > interval_seconds:
                filtered_time_series.append(end_frame_str)
                current_time = previous_time
            else:
                previous_time = None

    video_manager.release()
    return filtered_time_series


#定义截图函数
def screenshot(filtered_time_series):
    if not filtered_time_series:
        return
    for i, start_frame in enumerate(filtered_time_series):
        out_filename = 'output/scene_%03d.jpg'
        # 构建FFmpeg命令
        ffmpeg_cmd = [
            'ffmpeg',
            '-i', in_filename,  # 输入文件
            '-ss', str(start_frame),  # 开始时间
            '-frames:v', '1',  # 只取一帧
            '-filter:v', 'scale=-1:360',  # 缩放视频高度到480像素，宽度自动调整
            '-q:v', '2',  # JPEG质量设置
            '-y',
            out_filename  % i # 输出文件
        ]
        # 执行FFmpeg命令
        subprocess.run(ffmpeg_cmd)
        result = subprocess.run(ffmpeg_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        print(result.stdout)


def main():
    filtered_time_series = get_scene()
    screenshot(filtered_time_series)
    print("==ok==")


if __name__ == '__main__':
    main()

