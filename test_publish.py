import json
import os

import common
import requests

from server.RabbitMqConsumer import RabbitMqConsumer
queue_name = 'rtc_publisher_queue'
consumer = RabbitMqConsumer(queue_name)
data = {
    'scene_msg': 'xxxxxx',
    'scene_img': 'imgimig'
}
#json转字符串

consumer.publish(json.dumps(data))
