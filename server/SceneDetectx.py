import datetime
import os
import random
import subprocess
import time

from scenedetect import VideoManager, SceneManager, ContentDetector
import common


# 场景检测
class SceneDetector:
    def __init__(self, in_filename, create_at, callback):

        if os.path.exists(in_filename) is None:
            print(f"文件或目录 '{in_filename}' 不存在。")

        # 检查是否是一个文件
        if os.path.isfile(in_filename) is None:
            print(f" '{in_filename}' 不是一个文件。")

        self.in_filename = in_filename
        self.config = common.readConfig()
        self.api_key = self.config.get('chatgpt', 'zhipu_apikey')

        out_directory = self.config.get('scene_out_directory', 'out_root_directory')
        out_directory = out_directory + self.config.get('scene_out_directory', 'out_img_directory')
        out_directory = out_directory + datetime.datetime.now().strftime("%Y%m%d")
        common.create_directory(out_directory)
        self.create_at = create_at
        self.out_directory = out_directory
        self.video_manager = VideoManager(in_filename)
        self.scene_manager = SceneManager()
        self.scene_manager.add_detector(ContentDetector())
        self.rel_callback = callback

    def get_scene(self):
        self.video_manager.start()
        self.scene_manager.detect_scenes(frame_source=self.video_manager)
        scene_list = self.scene_manager.get_scene_list()
        # print("38 >>>>>>")
        # print(scene_list)
        if scene_list == []:
            print("没有检测到场景，取头帧")
        return scene_list

    # def get_scene2(self):
    #     ## 加入时间间隔
    #     self.video_manager.start()
    #     self.scene_manager.detect_scenes(frame_source=self.video_manager)
    #     scene_list = self.scene_manager.get_scene_list()
    #     time_format = "%H:%M:%S"
    #     filtered_time_series = []
    #     previous_time = None
    #     for i, scene in enumerate(scene_list):
    #         end_frame = str(scene[1])
    #         end_frame_str = end_frame.split('.')[0]
    #         if i == 0:
    #             current_time = datetime.datetime.strptime(end_frame_str, time_format)
    #         else:
    #             previous_time = datetime.datetime.strptime(end_frame_str, time_format)
    #
    #         if previous_time is not None:
    #             time_delta = current_time - previous_time
    #             if abs(time_delta.total_seconds()) > 60:
    #                 filtered_time_series.append(end_frame_str)
    #                 current_time = previous_time
    #             else:
    #                 previous_time = None
    #
    #     self.video_manager.release()
    #     return filtered_time_series

    def screenshot2(self, filtered_time_series):
        _now = datetime.datetime.now().strftime("%H%M%S")
        if (filtered_time_series is None or len(filtered_time_series) == 0):
            # 未检测到场景点
            print('line：79，未检测到场景点 >>>> ')
            random_number = random.randint(100, 999)
            out_filename = f'{self.out_directory}/scene_{_now}_{random_number}.jpg'
            print(' screenshot 输入的文件：')
            print(self.in_filename)
            print(' screenshot  输出的文件：')
            print(out_filename)
            # 下面不能用了，报错
            # ffmpeg_cmd = [
            #     'ffmpeg',
            #     '-loglevel panic -i', self.in_filename,
            #     '-frames:v', '1',
            #     '-filter:v', 'scale=-1:360',
            #     '-q:v', '2',
            #     '-y',
            #     out_filename
            # ]

            # ffmpeg_cmd = f'ffmpeg -loglevel panic -i "{self.in_filename}" -frames:v 1 -filter:v "scale=-1:360" -q:v 2 -y {out_filename}'

            # result = subprocess.run(ffmpeg_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

            ffmpeg_cmd = f'ffmpeg -loglevel panic -i "{self.in_filename}" -frames:v 1 -filter:v "scale=-1:360" -q:v 2 -y {out_filename}'
            subprocess.run(ffmpeg_cmd, shell=True, check=True)
            # print("ffmpeg_cmd >>>>")
            # print(ffmpeg_cmd)

            self.rel_callback(out_filename, self.in_filename, self.create_at)
            return
        else:
            for i, start_frame in enumerate(filtered_time_series):
                out_filename = f'{self.out_directory}/scene_{_now}_{i:03d}.jpg'
                # print("74 >>>>")
                # print(self.in_filename)
                # print("out_filename >>>>")
                # print(out_filename)
                # print("start_frame >>>>")
                # print( str(start_frame))

                ffmpeg_cmd = [
                    'ffmpeg',
                    '-loglevel panic -i', self.in_filename,
                    '-ss', str(start_frame),
                    '-frames:v', '1',
                    '-filter:v', 'scale=-1:360',
                    '-q:v', '2',
                    '-y',
                    out_filename
                ]

                result = subprocess.run(ffmpeg_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
                # print(result.stdout)
                # 等1s
                # subprocess.run(['sleep', '1'])
                self.rel_callback(out_filename, self.in_filename, self.create_at)
            return

    def screenshot(self, filtered_time_series):
        _now = datetime.datetime.now().strftime("%H%M%S")

        # 不检测场景点
        print('line：79，未检测到场景点 >>>> ')
        random_number = random.randint(100, 999)
        out_filename = f'{self.out_directory}/scene_{_now}_{random_number}.jpg'
        print(' screenshot 输入的文件：')
        print(self.in_filename)
        print(' screenshot  输出的文件：')
        print(out_filename)

        ffmpeg_cmd = f'ffmpeg -loglevel panic -i "{self.in_filename}" -frames:v 1 -filter:v "scale=-1:360" -q:v 2 -y {out_filename}'
        subprocess.run(ffmpeg_cmd, shell=True, check=True)
        # print("ffmpeg_cmd >>>>")
        # print(ffmpeg_cmd)

        self.rel_callback(out_filename, self.in_filename, self.create_at)
        return

    def process_video(self):
        filtered_time_series = self.get_scene()
        self.screenshot(filtered_time_series)
        print("== process_video end ==")
        print(' [*] Waiting for messages. To exit press CTRL+C')

# Usage
# if __name__ == '__main__':
#     in_filename = '../video/video.mp4'
#     out_directory = './output'
#     scene_detector = SceneDetector(in_filename, out_directory)
#     scene_detector.process_video()
